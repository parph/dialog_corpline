
var front_client, front_client_cont = false;
var header__menu_item, header__menu_item_id, header__menu_this;
var other_news, other_news_cont = false;
var front_product, front_product_cont= false;
var front_slider, front_slider_cont = false;
var geography_height, geography_width, about_geography;


function scroll_about_company(){
	if ($(".front-company").size()>0) {

		win_height = $(window).height();    
		var margin = $(".front-company").offset().top - win_height/2;    

		$(window).scroll(function(){
			if ($(this).scrollTop() > margin){  
				$('.front-company').addClass("front-company--anim");
			}
		});
	}
};

function client_front(){
		if (front_client_cont.size()>0) {
			var settings = [
				{
					minSlides: 6,  
					maxSlides: 6,
					slideWidth: 600,
					slideMargin: 0,
					pager: false,
					nextText: "",
					prevText: ""
				},
				{
					minSlides: 5,  
					maxSlides: 5,
					slideWidth: 600,
					slideMargin: 0,
					pager: false,
					nextText: "",
					prevText: ""
				},
				{
					minSlides: 4,  
					maxSlides: 4,
					slideWidth: 600,
					slideMargin: 0,
					pager: false,
					nextText: "",
					prevText: ""
				},
				{
					minSlides: 3,  
					maxSlides: 3,
					slideWidth: 600,
					slideMargin: 0,
					pager: false,
					nextText: "",
					prevText: ""
				},
				{
					minSlides: 2,  
					maxSlides: 2,
					slideWidth: 600,
					slideMargin: 0,
					pager: false,
					nextText: "",
					prevText: ""
				},
				{
					minSlides: 1,  
					maxSlides: 1,
					slideWidth: 600,
					slideMargin: 0,
					pager: false,
					nextText: "",
					prevText: ""
				}
			];
			
			if (Modernizr.mq('(min-width: 1251px)')){
				if(front_client){
					front_client.reloadSlider(settings[0]);
				} else {
					front_client = front_client_cont.bxSlider(settings[0]);
				}
			} else
			if (Modernizr.mq('(min-width: 992px)')){
				if(front_client){
					front_client.reloadSlider(settings[1]);
				} else {
					front_client = front_client_cont.bxSlider(settings[1]);
				}
			} else
			if (Modernizr.mq('(min-width: 768px)')){
				if(front_client){
					front_client.reloadSlider(settings[2]);
				} else {
					front_client = front_client_cont.bxSlider(settings[2]);
				}
			} else 
			if (Modernizr.mq('(min-width: 551px)')){
				if(front_client){
					front_client.reloadSlider(settings[3]);
				} else {
					front_client = front_client_cont.bxSlider(settings[3]);
				}
			} else 
			if (Modernizr.mq('(min-width: 401px)')){
				if(front_client){
					front_client.reloadSlider(settings[4]);
				} else {
					front_client = front_client_cont.bxSlider(settings[4]);
				}
			} else {
				if(front_client){
					front_client.reloadSlider(settings[5]);
				} else {
					front_client = front_client_cont.bxSlider(settings[5]);
				}
			}
		}
}

function product_front(){
		if (front_product_cont.size()>0) {
			var settings = [
				{
					slideMargin: 24,
					pager: false,
					controls: false,
					minSlides: 3,  
					maxSlides: 3,
					slideWidth: 600,
					ticker:true,
					speed: 50000
				},
				{
					slideMargin: 24,
					pager: false,
					controls: false,
					minSlides: 2,  
					maxSlides: 2,
					slideWidth: 600,
					ticker:true,
					speed: 50000
				},
				{
					slideMargin: 24,
					pager: false,
					controls: false,
					minSlides: 1,  
					maxSlides: 1,
					slideWidth: 600,
					ticker:true,
					speed: 50000
				}
			];
			
			if (Modernizr.mq('(min-width: 992px)')){
				if(front_product){
					front_product.reloadSlider(settings[0]);
				} else {
					front_product = front_product_cont.bxSlider(settings[0]);
				}
			} else
			if (Modernizr.mq('(min-width: 550px)')){
				if(front_product){
					front_product.reloadSlider(settings[1]);
				} else {
					front_product = front_product_cont.bxSlider(settings[1]);
				}
			} else {
				if(front_product){
					front_product.reloadSlider(settings[2]);
				} else {
					front_product = front_product_cont.bxSlider(settings[2]);
				}
			}
		}
}

function news_other(){
		if (other_news_cont.size()>0) {
			var settings = [
				{
					slideMargin: 20,
					pager: false,
					minSlides: 2,  
					maxSlides: 2,
					moveSlides: 1,
					infiniteLoop: false,
					slideWidth: 600,
					prevSelector: ".news-other__pager-item--prev",
					nextSelector: ".news-other__pager-item--next",
					onSlideBefore: function( $slideElement, oldIndex, newIndex){ 

					}
				},
				{
					slideMargin: 0,
					pager: false,
					prevSelector: ".news-other__pager-item--prev",
					nextSelector: ".news-other__pager-item--next"
				}
			];
			 
			if (Modernizr.mq('(min-width: 551px)')){
				if(other_news){
					other_news.reloadSlider(settings[0]);
				} else {
					other_news = other_news_cont.bxSlider(settings[0]);
				}
			} else {
				if(other_news){
					other_news.reloadSlider(settings[1]);
				} else {
					other_news = other_news_cont.bxSlider(settings[1]);
				}
			}
		}
}
function front_slider() {
	if ( front_slider_cont.size()>0) {
			var frontSliderTimer, about_animate=false, active_page;
			var loadTime = 6000;
			function frontSliderNextTimer(active_page){
				clearTimeout(frontSliderTimer);
				if (about_animate) { about_animate.stop(); }
					frontSliderTimer = setTimeout(
						function(){ 
							active_page.find(".dial").knob({
							width: 32,
							height: 32,
							readOnly: true,
							displayInput: true,
							fgColor: '#fff',
							bgColor: 'transparent',
							thickness: .1
						});

						/* анимация кружка навигации */
						about_animate = $({animatedVal: 0}).animate(
							{animatedVal: 100},
							{
								duration: loadTime,
								easing: "swing",
								step: function() { 
									active_page.find(".dial").val(Math.ceil(this.animatedVal)).trigger("change"); 
								},
								complete: function(){ 
									active_page.find(".dial").val("0").trigger("change");
								}
							}
						); 
				}, 200 );
			}

		var settings = [
			{
				mode: "fade",
				slideMargin: 0,
				pagerCustom: '.front-slider__pagin',
				auto: true,
				pause: 6000,
				onSliderLoad: function(){ 
					console.log("onSliderLoad");
							$(".front-slider").css({"opacity":"1"});
							$('.front-slider__fone-item').removeClass('visible');
							$('.front-slider__fone-item[data-frontslide=0]').addClass('visible');

							$(".front-slider__pagin .front-slider__pagin-item[data-slide-index=0]").html('<input type="text" value="0" class="dial" data-readOnly="true">');

							active_page = $(".front-slider__pagin .front-slider__pagin-item[data-slide-index=0]");
							frontSliderNextTimer(active_page);
				},
				onSlideBefore: function( $slideElement, oldIndex, newIndex){ 
					console.log("onSlideBefore");
							if($('.front-slider__fone-item[data-frontslide='+newIndex+']').length){
									$('.front-slider__fone-item').removeClass('visible');
									$('.front-slider__fone-item[data-frontslide='+newIndex+']').addClass('visible');
							}

							$(".front-slider__pagin .front-slider__pagin-item").html("");
							$(".front-slider__pagin .front-slider__pagin-item[data-slide-index='"+newIndex+"']").html('<input type="text" value="0" class="dial" data-readOnly="true">');

							active_page = $(".front-slider__pagin .front-slider__pagin-item[data-slide-index='"+newIndex+"']");
							frontSliderNextTimer(active_page);
				}
			}
		];

		if(other_news){
			front_slider.reloadSlider(settings[0]);
		} else {
			front_slider = front_slider_cont.bxSlider(settings[0]);
		}

		$(".front-slider .bx-prev").click(function(){
			front_slider.stopAuto();
			front_slider.startAuto();
		});
		$(".front-slider .bx-next").click(function(){
			front_slider.stopAuto();
			front_slider.startAuto();
		});
		$(".front-slider__pagin-item").click(function(){
			front_slider.stopAuto();
			front_slider.startAuto();
		});


		$(".front-slider__item-text").hover(
			function(){
				$(".front-slider__fone").addClass("dark");
			},
			function(){
				$(".front-slider__fone").removeClass("dark");
			}
		)
	}
}


function support(){
	if ( $(".support__slider").size()>0) {
		$( "#packages_EDO_support" ).slider({
		  value:100,
		  min: 50,
		  max: 150,
		  step: 50,
		  slide: function( event, ui ) {
			$( "#packages_EDO" ).val( "$" + ui.value );
			$("#packages_EDO_text").text(ui.value);
		  }
		});
		$( "#packages_EDO" ).val( "$" + $( "#packages_EDO_support" ).slider( "value" ) );
		$("#packages_EDO_text").text($( "#packages_EDO_support" ).slider( "value" ));

		$( "#consultancy_support" ).slider({
		  value:90,
		  min: 90,
		  max: 120,
		  step: 30,
		  slide: function( event, ui ) {
			$( "#consultancy" ).val( "$" + ui.value );
			$("#consultancy_text").text(ui.value);
		  }
		});
		$( "#consultancy" ).val( "$" + $( "#consultancy_support" ).slider( "value" ) );
		$("#consultancy_text").text($( "#consultancy_support" ).slider( "value" ));

		$( "#ulegal_entity_support" ).slider({
		  value: 1,
		  min: 1,
		  max: 2,
		  step: 1,
		  slide: function( event, ui ) {
			$( "#ulegal_entity" ).val( "$" + ui.value );
			$("#ulegal_entity_text").text(ui.value);
		  }
		});
		$( "#ulegal_entity" ).val( "$" + $( "#ulegal_entity_support" ).slider( "value" ) );
		$("#ulegal_entity_text").text($( "#ulegal_entity_support" ).slider( "value" ));
	}
}

function contact(){
	if ($("#contact_map").size()>0) {
		contact_map = new ymaps.Map("contact_map", {
				center: [59.980224, 30.334808],
				zoom: 17,
				controls: []
		});
		
		contact_map.behaviors.disable('scrollZoom');
		//contact_map.behaviors.disable('multiTouch');

		contact_map.controls.add('zoomControl');
		var myPlacemark = new ymaps.Placemark([59.980224, 30.334808] , {},
			{   iconLayout: 'default#image',
				iconImageHref: 'images/mappin_ico.svg',
				iconImageSize: [72, 87],
				iconImageOffset: [-36, -87] 
			});
		
		contact_map.geoObjects.add(myPlacemark);
	}
}
function form(){
	if ($(".form__select").size()>0) {
		console.log("select");
		//$(".form__select select").chosen({disable_search_threshold: 10});
		$(".form__select select").styler();
	}

	$(".phone").mask("8 (999) 999-9999");

	$(".form__text input").focus(function(){
		$(this).next().addClass("placeholder-hide");
	});
	$(".form__text input").blur(function(){
		if($(this).val()=="") {
			$(this).next().removeClass("placeholder-hide");
		}    
	});

	$(".form__textarea textarea").focus(function(){
		$(this).next().addClass("placeholder-hide");
	});
	$(".form__textarea textarea").blur(function(){
		if($(this).val()=="") {
			$(this).next().removeClass("placeholder-hide");
		}    
	});
}

function product() {
	$(".product__tabs-item").click(function(e){
		if(!$(this).hasClass("active")) {
			var text_id = $(this).attr("href");
			if( $(text_id).size()>0) {
				$(".product__tabs-item").removeClass("active")
				$(this).addClass("active");
				$(".product__blocks-item").hide();
				$(text_id).show();
				e.preventDefault();
			}
		}
	})

	if( $(".product-list").size()>0) {
		$(".block-type__item--block").click(function(e){
			if (!$(".block-type__item--block").hasClass("active")) {
				$(".block-type__item").removeClass("active");
				$(this).addClass("active");
				$(".product-list").removeClass("product-list--line");
			}
			e.preventDefault();
		});
		$(".block-type__item--line").click(function(e){
			if (!$(".block-type__item--line").hasClass("active")) {
				$(".block-type__item").removeClass("active");
				$(this).addClass("active");
				$(".product-list").addClass("product-list--line");
			}
			e.preventDefault();
		})
	}
}

function clients(){
	if( $(".clients").size()>0) {
		$(".block-type__item--block").click(function(e){
			if (!$(".block-type__item--block").hasClass("active")) {
				$(".block-type__item").removeClass("active");
				$(this).addClass("active");
				$(".clients").removeClass("clients--line");
			}
			e.preventDefault();
		});
		$(".block-type__item--line").click(function(e){
			if (!$(".block-type__item--line").hasClass("active")) {
				$(".block-type__item").removeClass("active");
				$(this).addClass("active");
				$(".clients").addClass("clients--line");
			}
			e.preventDefault();
		})
	}
}

function header_menu(){
	$(".header__menu-item>a, .header__menu-item>span.parent").mouseenter(function(){
		if (Modernizr.mq('(min-width: 768px)')) {
			header__menu_item_id = "#" + $(this).attr("data-id");
			header__menu_this = $(this);
			clearTimeout(header__menu_item);
			header__menu_item = setTimeout(function(){
				if ($(header__menu_item_id).size()>0) {
					if ( !$(".header-menu2lv").hasClass("open")) {
						$(".header-menu2lv").addClass("open").stop().fadeIn();
					}
					if ( $("body").hasClass("front") && !$("front-slider").hasClass("blur")  ) {
						$(".front-slider").addClass("blur");
					}
					/*console.log(header__menu_item_id);*/
					if ($(".header-menu2lv__item.open").size()==0) {
						$(header__menu_item_id).stop().addClass("open").fadeIn(300);
					}           
					if( !( $(header__menu_item_id).hasClass("open") && $(".header-menu2lv__item.open").size()==1) ){
						$(".header-menu2lv__item").removeClass("open").stop().fadeOut(300);
						setTimeout(function(){ $(header__menu_item_id).stop().addClass("open").fadeIn(300); },300);
					}
					$(".header__menu-item>a.parent, .header__menu-item>span.parent").removeClass("open");
					header__menu_this.addClass("open");
				}
				else {
					$(".header__menu-item>a.parent, .header__menu-item>span.parent").removeClass("open");
					$(".header-menu2lv__item").stop().removeClass("open").fadeOut(300);
					$(".header-menu2lv").removeClass("open").stop().fadeOut(); 
					if ( $("body").hasClass("front") ) {
						$(".front-slider").removeClass("blur");
					}
				}
			}, 200);

		}
	});

	$(".header__menu-item>a, .header__menu-item>span.parent").on("touchend",function(e){
		if (Modernizr.mq('(min-width: 768px)')) {
			if ( !$(this).hasClass("open")) {
				header__menu_item_id = "#" + $(this).attr("data-id");
				if ($(header__menu_item_id).size()>0) {
					if ( !$(".header-menu2lv").hasClass("open")) {
						$(".header-menu2lv").addClass("open").stop().fadeIn();
					}
					if ( $("body").hasClass("front") && !$("front-slider").hasClass("blur")  ) {
						$(".front-slider").addClass("blur");
					}
					/*console.log(header__menu_item_id);*/
					if ($(".header-menu2lv__item.open").size()==0) {
						$(header__menu_item_id).stop().addClass("open").fadeIn(300);
					}           
					if( !( $(header__menu_item_id).hasClass("open") && $(".header-menu2lv__item.open").size()==1) ){
						$(".header-menu2lv__item").removeClass("open").stop().fadeOut(300);
						setTimeout(function(){ $(header__menu_item_id).stop().addClass("open").fadeIn(300); },300);
					}
					$(".header__menu-item>a.parent, .header__menu-item>span.parent").removeClass("open");
					$(this).addClass("open");
				}
				else {
					$(".header__menu-item>a.parent, .header__menu-item>span.parent").removeClass("open");
					$(".header-menu2lv__item").stop().removeClass("open").fadeOut(300);
					$(".header-menu2lv").removeClass("open").stop().fadeOut(); 
					if ( $("body").hasClass("front") ) {
						$(".front-slider").removeClass("blur");
					}
				}
				
				e.preventDefault();
			}
		}
	});


	$(".header__top-adap").click(function(){
		$(".header__bottom").addClass("open");
	});
	$(".header__bottom-close").click(function(){
		$(".header__bottom").removeClass("open");
	});

	$(".header-menu2lv__close").click(function(){
		$(".header-menu2lv__item").stop().removeClass("open").fadeOut(300);
		$(".header-menu2lv").removeClass("open").stop().fadeOut(); 
		if ($("body").hasClass("front")) {
			$(".front-slider").removeClass("blur");
		}
		
	});

	$(".header__bottom").mouseleave(function(){
		clearTimeout(header__menu_item);
		$(".header__menu-item>a.parent, .header__menu-item>span.parent").removeClass("open");
		if ($(".header-menu2lv").hasClass("open")) {
			$(".header-menu2lv__item").stop().removeClass("open").fadeOut(300);
			$(".header-menu2lv").removeClass("open").stop().fadeOut(); 
			if ($("body").hasClass("front")) {
				$(".front-slider").removeClass("blur"); 
			}
		}
	});

	$(".header__top-adap-btn").click(function(){
		if( $(this).hasClass("open")) {
			$(this).removeClass("open");
			$(".header__top-adap-menu").stop().fadeOut();
		} else {
			$(this).addClass("open");
			$(".header__top-adap-menu").stop().fadeIn();
		}
	});

	$(".header__bottom-adap").click(function(){
		if($(this).hasClass("open")) {
			$(".header-menu2lv").stop().slideUp();
			$(this).removeClass("open");
			$(".header-menu2lv__list").slideUp();
			$(".header-menu2lv__title").removeClass("open");
			$(".header-menu2lv__item").hide();
		}
		else{
			$(".header-menu2lv__item").show();
			$(".header-menu2lv").stop().slideDown();
			$(this).addClass("open");
		}
	});
	$(".header-menu2lv__title").click(function(e){
		if (Modernizr.mq('(max-width: 767px)') && ($(this).hasClass("header-menu2lv__title--parent"))){
			if ( !$(this).hasClass("open")) {
				$(".header-menu2lv__list").slideUp();
				$(".header-menu2lv__title").removeClass("open");
				$(this).next().slideDown();
				$(this).addClass("open");
				e.preventDefault();
			}
		}
	});
}

function sort() {
	$(".sort__adap").click(function(){
		if (Modernizr.mq('(max-width: 991px)')){
			if ($(this).hasClass("active")) {
				$(".sort__adap").removeClass("active")
				$(".sort form").slideUp();
			}
			else {
				$(".sort__adap").addClass("active")
				$(".sort form").slideDown();
			}
		}
	});

	$(".sort__clear").click(function(e){
		var id_block = $(this).attr("href");
		if($(id_block).size()>0) {
			$(id_block).find("input").each(function(){
				$(this).removeAttr("checked");
			})
		}
		e.preventDefault();
	});

	$(".sort__btn").click(function(e){
		$(".sort input").each(function(){
			$(this).removeAttr("checked");
		});
		e.preventDefault();
	})
}

function form_popup(){
	$('.call-me').click(function(){
		$(".wrapper").addClass("wrapper--move");
		$(".form-popup--call-me").addClass("form-popup--open");
	});

	$(".invitation__btn").click(function(evt){
		$(".wrapper").addClass("wrapper--move");
		$(".form-popup--invitation").addClass("form-popup--open");
		evt.preventDefault();
	})

	$(".form-popup__close, .form-popup__fone").click(function() {
			$(".wrapper").removeClass("wrapper--move");
			$(".form-popup").addClass("form-popup--close");
			setTimeout(function(){ $(".form-popup").removeClass("form-popup--open"); $(".form-popup").removeClass("form-popup--close");},200)      
	});
}

function geography_map(){
	if ($(".geography__cont").size()>0) {
		geography_width=$(".geography__cont").height()/0.5287;
		geography_height=$(".geography__cont").height();
		$(".geography__cont-img").css({"width":geography_width, "height":geography_height});
		$(".geography__cont-img svg").css({"width":geography_width, "height":geography_height});
	}
}

function geography_map_move(){
	about_geography = $(".geography__cont");

	if (about_geography.size()>0) {
		/* Mouse dragg scroll */
		var x, y, top, left, down=false;
		about_geography.attr("onselectstart", "return false;");   // Disable text selection in IE8
		about_geography.addClass('dragscroll-active');
		//about_geography.css('overflow','auto');
		about_geography.css('-webkit-overflow-scrolling','touch');
		about_geography.attr("data-top", 0);
		about_geography.attr("data-left", 0);

		function dragmove(evt){
			if (down) {
				if (evt.type == "touchmove") {
				  var touch = evt.originalEvent.touches[0] || evt.originalEvent.changedTouches[0];
				  var newX = touch.pageX;
				  var newY = touch.pageY;
				}
				else {
				  var newX = evt.pageX;
				  var newY = evt.pageY;
				}
				
				about_geography.scrollTop(top - newY + y);
				about_geography.scrollLeft(left - newX + x);
				about_geography.addClass('dragscroll-mousemove');
				about_geography.attr("data-top", top);
				about_geography.attr("data-left", left);
			}

		}		
		
		function dragdown(evt){
		  
			if (evt.type == "touchstart") {
				var touch = evt.originalEvent.touches[0] || evt.originalEvent.changedTouches[0];
				x = touch.pageX;
				y = touch.pageY;
			}
			else {
			  x = evt.pageX;
			  y = evt.pageY;
			}

			down = true;
			top = about_geography.scrollTop();
			left = about_geography.scrollLeft();
			about_geography.addClass('dragscroll-mousedown');
			about_geography.attr("data-top", top);
			about_geography.attr("data-left", left);

		}		
		
		function dragend(evt){
			down = false; 
		  about_geography.removeClass('dragscroll-mousedown');
		  about_geography.removeClass('dragscroll-mousemove');
		}		

		about_geography.on('mousedown', function(evt){
			dragdown(evt);			
			evt.preventDefault();
		});
		about_geography.on('mousemove', function(evt){
	 	    dragmove(evt);
			evt.preventDefault();
		});
			
		about_geography.on('mouseup', function(evt){
			dragend(evt);
			evt.preventDefault();
		});

		about_geography.on('touchstart', function(evt){
			dragdown(evt);			
		});
		about_geography.on('touchmove', function(evt){
	 	    dragmove(evt);
		});
			
		about_geography.on('touchend', function(evt){
			dragend(evt);
		});

		about_geography.on('mouseleave', function(evt){
			down = false;
			about_geography.removeClass('dragscroll-mousedown');
			about_geography.removeClass('dragscroll-mousemove');
		});	
	}
}


$(document).ready(function(){
	win_height = $(window).height();
	win_width  = $(window).width();
});

$(window).load(function(){
	geography_map();
	geography_map_move();

	/*$("#Moskva").click(function(){*/
	$("#Moskva").on( 'click', function(){
		var top = $(this).offset().top - about_geography.offset().top + parseInt($(".geography__cont").attr("data-top"));
		var left = $(this).offset().left - about_geography.offset().left + parseInt($(".geography__cont").attr("data-left")) ;

		if (Modernizr.mq('(min-width: 1701px)')){
			top-=110;
			left+=10;
		} else
		if (Modernizr.mq('(min-width: 1351px)')){
			top-=110;
			left+=10;
		} else 
		if (Modernizr.mq('(min-width: 992px)')){
			top-=115;
			left+=5;
		} else if (Modernizr.mq('(min-width: 767px)')){
			top-=115;
			left+=5;
		} else {
			top-=115;
			left+=5;
		}

		//console.log("width "+$(this).find("path").width()/2+"; height"+$(this).find("path").height()/2 );

		$(".geography__pin").css({"top":top, "left":left}).fadeIn();
	});
	$(".geography__pin-close").click(function(evt){
		$(this).parent().fadeOut();
		evt.preventDefault();
	})




	support();
	contact();
	form();

	$(".fancybox").fancybox();

	front_slider_cont = $(".front-slider__list");
	front_slider();
	
	product();
	clients();
	header_menu();

	front_client_cont = $('.front-client__slider');
	if (front_client_cont.size()>0){
		client_front();
	}
	
	other_news_cont = $('.news-other__list');
	news_other();

	front_product_cont = $(".front-product-list");
	product_front();
	
	form_popup();	
	scroll_about_company();	


	$(document).mouseup(function (evt){ // событие клика по веб-документу
		if (Modernizr.mq('(min-width: 768px)')) {
			var header__menu = $(".header__bottom"); // тут указываем ID элемента
			if (!header__menu.is(evt.target) // если клик был не по нашему блоку
					&& header__menu.has(evt.target).length === 0) { // и не по его дочерним элементам
					$(".header-menu2lv__item").stop().removeClass("open").fadeOut(300);
					$(".header-menu2lv").removeClass("open").stop().fadeOut(); 
					if ( $("body").hasClass("front") ) {
						$(".front-slider").removeClass("blur");
					}
					clearTimeout(header__menu_item);
			}
		}

		if (!about_geography.is(evt.target) // если клик был не по нашему блоку
				&& about_geography.has(evt.target).length === 0) { // и не по его дочерним элементам
				down = false; 
				about_geography.removeClass('dragscroll-mousedown');
				about_geography.removeClass('dragscroll-mousemove');
		}
	});

	
})

$(window).resize(function(){
	var win_height_n = $(window).height();
	var win_width_n = $(window).width();

	if ( (Math.abs( win_height_n - win_height ) > 50)  ||  win_width_n!=win_width ) {

		client_front();
		news_other();
		product_front();

		if ($(".sort").size()>0) {
			if (Modernizr.mq('(min-width: 991px)')){
				$(".sort form").show();
				if ($(".sort__adap").hasClass("active")) {
					$(".sort__adap").removeClass("active")
				}
			} else {
				$(".sort form").hide();
				$(".sort__adap").removeClass("active")
			} 
		}

		geography_map();
	}	
	
	if (Modernizr.mq('(min-width: 600px)')){
		$(".header__top-adap-menu").show();
	} else {
		$(".header__top-adap-menu").hide();
		if( $(".header__top-adap-btn").hasClass("open")) {
			$(".header__top-adap-btn").removeClass("open");
		}
	}

	$(".geography__pin").hide();

});



