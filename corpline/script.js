
var solutions, solutions_cont = false;
$(window).load(function(){
	solutions_cont = $('.solutions-steps__list');
	solutions_other();

})

function solutions_other(){
		if (solutions_cont.size()>0) {
			var settings = [
				{
					slideMargin: 0,
					pager: false,
					minSlides: 1,  
					maxSlides: 1,
					prevSelector: ".solutions-steps__pager-item--prev",
					nextSelector: ".solutions-steps__pager-item--next"
				},
				{
					slideMargin: 70,
					pager: false,
					minSlides: 1,  
					maxSlides: 2,
					moveSlides: 1,
					infiniteLoop: false,
					slideWidth: 320,
					prevSelector: ".solutions-steps__pager-item--prev",
					nextSelector: ".solutions-steps__pager-item--next"
				},
				{
					slideMargin: 100,
					pager: false,
					minSlides: 1,  
					maxSlides: 3,
					moveSlides: 1,
					infiniteLoop: false,
					slideWidth: 320,
					prevSelector: ".solutions-steps__pager-item--prev",
					nextSelector: ".solutions-steps__pager-item--next"
				}
			];
			 
			if (Modernizr.mq('(max-width: 767px)')){
				if(solutions){
					solutions.reloadSlider(settings[0]);
				} else {
					solutions = solutions_cont.bxSlider(settings[0]);
				}
			} else if (Modernizr.mq('(min-width: 768px) and (max-width: 991px)')){
				if(solutions){
					solutions.reloadSlider(settings[1]);
				} else {
					solutions = solutions_cont.bxSlider(settings[1]);
				}
			} else {
				if(solutions){
					solutions.reloadSlider(settings[2]);
				} else {
					solutions = solutions_cont.bxSlider(settings[2]);
				}
			}
		}
}